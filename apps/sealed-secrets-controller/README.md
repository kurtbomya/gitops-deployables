# Installation of Sealed Secrets Controller

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm template --dry-run sealed-secrets-controller -n kube-system -f ./values.yaml bitnami/sealed-secrets --output-dir ./
```
